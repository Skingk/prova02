package Controle;

import java.util.ArrayList;

import Models.Critica;

public class ControleCriticas {
    private ArrayList<Critica> listaCriticas;

    public ControleCriticas() {
        listaCriticas = new ArrayList<Critica>();
    }

    public ArrayList<Critica> getListaCriticas() {
        return listaCriticas;
    }

    public void setListaCriticas(ArrayList<Critica> listaCriticas) {
        this.listaCriticas = listaCriticas;
    }
    
    public void adicionaCritica(Critica texto){
    listaCriticas.add(texto);
    }
    
    public void removerCritica(Critica texto){
    listaCriticas.remove(texto);
    }
    
    public Critica pesquisaCritica(String umTipo){
    	for (Critica umaCritica : listaCriticas){
    		if (umaCritica.getTipo().equalsIgnoreCase(umTipo)){
    			return umaCritica;
    		}
    	}
    	return null;
    }
}
   
