package Controle;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

import Models.Critica;

public class ControleCriticasTest {
	static ControleCriticas umControleCriticas;
	static Critica umaCritica;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		umControleCriticas = new ControleCriticas();
		umaCritica = new Critica("contra");
	}

	@Test
	public void adicionaTest() {
		umControleCriticas.adicionaCritica(umaCritica);
		assertNotNull(umControleCriticas.pesquisaCritica("contra"));
		
	}
	
	@Test
	public void removerTest() {
		umControleCriticas.removerCritica(umaCritica);
		assertNull(umControleCriticas.pesquisaCritica("contra"));
		
	}
	
	@Test
	public void pesquisarTest() {
		umControleCriticas.pesquisaCritica(umTipo);
		assertNotNull(umControleCriticas.pesquisaCritica("contra"));
		
	}
	
}
