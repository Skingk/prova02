package prova02.Models;

public class Moderador {
    private String codigo;

    public Moderador() {
    }

    public Moderador(String codigo) {
        this.codigo = codigo;
    }
    

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    
    
}
