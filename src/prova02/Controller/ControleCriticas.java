package prova02.Controller;

import java.util.ArrayList;
import prova02.Models.Critica;

public class ControleCriticas {
    private ArrayList<Critica> listaCriticas;

    public ControleCriticas() {
        listaCriticas = new ArrayList<Critica>();
    }

    public ArrayList<Critica> getListaCriticas() {
        return listaCriticas;
    }

    public void setListaCriticas(ArrayList<Critica> listaCriticas) {
        this.listaCriticas = listaCriticas;
    }
    
    public void adicionaCritica(Critica texto){
    listaCriticas.add(texto);
    }
    
    public void removerCritica(Critica texto){
    listaCriticas.remove(texto);
    }
    
}
